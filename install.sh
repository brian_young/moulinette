#!/bin/bash
#################################################################################
## install for Moulinette de norme Epitech				       ##
##									       ##
## Made by de-dum_m & armita_a						       ##
## Login   <de-dum_m@epitech.net et armita_a@epitech.net>		       ##
#################################################################################

WHITE="\033[0m"
RED="\033[0;31m"
GREEN="\033[0;32m"
YELLOW="\033[0;33m"
BLUE="\033[0;34m"
MAGENTA="\033[0;35m"
CYAN="\033[0;36m"

BOLD="\033[1m"
BRED="\033[1;31m"
BGREEN="\033[1;32m"
BYELLOW="\033[1;33m"
BBLUE="\033[1;34m"
BMAGENTA="\033[1;35m"
BCYAN="\033[1;36m"

echo -e $BCYAN"Installation de la Moulinette de norme en cours"$WHITE

test $HOME/.config/norme || mkdir -p $HOME/.config/norme 
test $HOME/.config/norme/malloc || mkdir -p $HOME/.config/norme/malloc
cp moulinette ./norme
chmod +x ./norme
mv norme $HOME/bin/
echo "export PATH=$PATH:$HOME/bin" >> $HOME/.bashrc
cd
echo -e $BGREEN"installation terminé!!"$WHITE
echo "Si vous utilisez bash  : norme"
echo "Sinon l'executable est dans $HOME/bin"
