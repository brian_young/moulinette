# [EPITECH DEV TEAM](http://bitbucket.org/epitechdevteam) - MOULINETTE DE NORME
## Language : Bash
### Par Aljosha Armitano & Maxime De-Dumast


Executer dans le repertoire principal, de preference en
présence d'un Makefile et d'un executable.  
  

```
options:  
-h	  --help		afficher ce message
-c	  --no-colors		desactiver les couleurs
-u	  --users         permet d'ajouter des participants au projet
-M	  --makefile      activer la verification du Makefile
-m	  --malloc        desactiver la verification des mallocs
-C	  --comment        desactiver la verification des commentaire
-R	  --reset	   	      oublier les fonctions autorisees  
```

---  
  
# Installation  
## Dépendances:   
  - pcregrep   
## Installer pcregrep d'abord  
### Non root:
```
test -d $HOME/bin || mkdir -p $HOME/bin  
cd $HOME/bin  
wget "http://downloads.sourceforge.net/project/pcre/pcre/8.34/pcre-8.34.tar.bz2?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fpcre%2Ffiles%2Fpcre%2F&ts=1392562437&use_mirror=netcologne" -O pcre.tar.bz2  
tar -xf !$  
cd pcre-8.34  
./configure --prefix=$HOME/bin  
make  
echo "export PATH=$PATH:$PWD" >> $HOME/.bashrc  
cd  
echo "Installation terminée."  
  
```  
### root:  

```
sudo zypper install pcre-tools  
    
```
## Puis la moulinette
```
cd $HOME/bin
git clone https://bitbucket.org/epitechdevteam/moulinette.git
cd moulinette
./install.sh
  
```


---
Gérés:  

+ fonctions de plus de 25 lignes  
+  mauvais header  
+ fonctions autorisées/interdites  
+ nombre de fonctions par fichier  
+ whitespace en fin de ligne  
+ espace manquant après mot clef  
+ double retour à la ligne  
+ ligne de plus de 80 caractères  
+ plus de 4 paramètres pour une fonction  
+ commentaire dans le code  
+ ordonnencement des includes  
+ declaration et affectation sur la même ligne  
+ protection headers  
+ multi instruction  
+ define dans un .c  
+ norme Makefile  
+ retour à la ligne manquant entre deux fonctions  
+ espace en fin de ligne  
+ tabulation dans les déclarations
+ saut de ligne après les déclarations  
+ nominations des variables et structures  
+ verification malloc  
  
Non-gérés:  

+ espaces après opérateurs  
+ indentation générale/locale  
+ macro multiligne  
+ espace apres parenthese  

Sauvergarde les fonctions autorisées dans le repertoire:  
`$HOME/.config/norme`


> Il en manque beaucoup, donc n'hésitez pas a faire des "pull requests" si vous avez quelque chose à ajouter.
> Remontez-nous les faux positifs/négatifs ou autres bugs, ou mieux corrigez les et on mergera.
> 
> Email : <de-dum_m@epitech.eu> ou  <armita_a@epitech.eu>
> 
> Signalement de bugs : [ICI](https://bitbucket.org/epitechdevteam/moulinette/issues?status=new&status=open)
